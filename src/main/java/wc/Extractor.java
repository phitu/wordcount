package wc;

import java.util.ArrayList;
import java.util.List;

public class Extractor {

    public List<String> splitSentence(String sentence) {
        List<String> listOfStrings = new ArrayList<String>();
        String[] words = sentence.toLowerCase().split(" ");
        for(String str : words) {
            listOfStrings.add(removeSpecialCharacters(str));
        }
        return listOfStrings;
    }

    private String removeSpecialCharacters(String word) {
        return word.replaceAll("[,.?!;]*", "");
    }
}
