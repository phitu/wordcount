package wc;

public class App {
    public static void main(String[] args) {
        if(args.length !=0) {
            new WordCount(new Extractor()).countWords(args[0]);
        }
    }
}
