package wc;

import org.junit.platform.commons.util.StringUtils;

import java.util.*;

public class WordCount {

    private Extractor extractor;

    public WordCount(Extractor extractor) {
        this.extractor = extractor;
    }

    public void countWords(String sentence) {
        if(!StringUtils.isBlank(sentence)) {
            Map<String, Integer> wordOccs = createWordOccurances(sentence);
            for(Map.Entry<String, Integer> word : wordOccs.entrySet()) {
                System.out.println(word.getKey() + " : " +word.getValue());
            }
        }
    }

    public Map<String, Integer> createWordOccurances(String sentence) {
        List<String> listOfWords = extractor.splitSentence(sentence);
        
        Set<String> uniqueWords= new HashSet<>(listOfWords);
        Map<String, Integer> stringOccurences = createStringOccurences(uniqueWords);
        for(String str : stringOccurences.keySet()) {
            int count = Collections.frequency(listOfWords, str);
            stringOccurences.put(str, count);
        }

        return stringOccurences;
    }
    
    private Map<String, Integer> createStringOccurences(Set<String> words) {
        Map<String, Integer> stringOccurences = new HashMap<>();
        for(String str : words) {
            stringOccurences.put(str, 0);
        }
        return stringOccurences;
    }

}
