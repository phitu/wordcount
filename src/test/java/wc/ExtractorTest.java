package wc;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertThat;

public class ExtractorTest {

    private Extractor extractor = new Extractor();

    @Test
    public void splitWord() {
        String sentence = "this is a word a sentence";
        List<String> words = extractor.splitSentence(sentence);

        assertThat(words.size(), Is.is(6));
    }

    @Test
    public void removeSpecialCharacters() {
        String sentence = "this, hello.";
        List<String> words = extractor.splitSentence(sentence);
        assertThat(words.contains("this"), Is.is(true));
        assertThat(words.contains("hello"), Is.is(true));
    }
}
