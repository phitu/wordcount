package wc;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class WordCountTest {

    private WordCount wc = new WordCount(new Extractor());

    @Test
    public void mapOfStringOccurencesReturnsFrequencyOfWords() {
        String sentence = "This is a word, this sentence";
        Map<String, Integer> wordOcc = wc.createWordOccurances(sentence);
        Assert.assertThat(wordOcc.keySet().size(), Is.is(5));

        Assert.assertThat(wordOcc.get("this"), Is.is(2));
        Assert.assertThat(wordOcc.get("is"), Is.is(1));
        Assert.assertThat(wordOcc.get("a"), Is.is(1));
        Assert.assertThat(wordOcc.get("word"), Is.is(1));
        Assert.assertThat(wordOcc.get("sentence"), Is.is(1));
    }

    @Test
    public void caseSentiveLettersReturnsCorrectNumberOfWord() {
        String sentence = "This is a word. This sentence, this word";
        Map<String, Integer> wordOcc = wc.createWordOccurances(sentence);
        Assert.assertThat(wordOcc.keySet().size(), Is.is(5));

        Assert.assertThat(wordOcc.get("this"), Is.is(3));
        Assert.assertThat(wordOcc.get("is"), Is.is(1));
        Assert.assertThat(wordOcc.get("a"), Is.is(1));
        Assert.assertThat(wordOcc.get("word"), Is.is(2));
        Assert.assertThat(wordOcc.get("sentence"), Is.is(1));
    }

    @Test
    public void ubsTest() {
        String sentence = "This is a statement, and so is this.";
        wc.countWords(sentence);
    }
}
